import { Component, ViewChild } from '@angular/core';
import { Chart, ChartConfiguration } from 'chart.js';
import Annotation from 'chartjs-plugin-annotation';
import { BaseChartDirective } from 'ng2-charts';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { TickerData } from 'src/app/models/ticker-data.model';
import { MarketDataService } from 'src/app/services/market-data.service';
import { TickerService } from 'src/app/services/ticker.service';

@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.css'],
})
export class MarketComponent {
  constructor(
    private marketDataService: MarketDataService,
    private tickerService: TickerService
  ) {
    Chart.register(Annotation);
  }

  data = new BehaviorSubject<TickerData>({});

  ngOnInit() {
    this.tickers$ = this.tickerService.getTickers();

    this.tickers$.subscribe((r) => {
      r.forEach((element: any) => {
        this.fetchTickerData(element.tickerLable);
      });
    });

    this.data.subscribe((response) => {
      this.chart?.update();
    });

    this.marketDataService.getTickerData().subscribe((response) => {
      this.data.next(response);
    });
  }

  tickers$: Observable<any> = of([]);

  fetchTickerData(ticker: string): void {
    this.marketDataService.createNewConnection(ticker);
  }

  getData(symbol: string) {
    return {
      labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
      datasets: [
        {
          data: this.data.value[symbol],
          label: symbol,
          fill: true,
          tension: 0,
          borderColor: '#7856CE',
          backgroundColor: 'rgba(164, 129, 254, 0.3)',
          pointBorderColor: 'grey',
          pointBackgroundColor: '#7856CE',
        },
      ],
    };
  }

  @ViewChild(BaseChartDirective) chart?: BaseChartDirective;
}
