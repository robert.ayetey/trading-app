import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreatePortfolioComponent } from 'src/app/components/create-portfolio/create-portfolio.component';
import { PortfolioService } from 'src/app/services/portfolio.service';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ExistingPortfolio } from 'src/app/models/portfolio.model';
import { UiService } from 'src/app/services/ui.service';
import { HoldingService } from 'src/app/services/holding.service';

@Component({
  selector: 'app-client-dashboard',
  templateUrl: './client-dashboard.component.html',
  styleUrls: ['./client-dashboard.component.css'],
})
export class ClientDashboardComponent implements OnInit {
  constructor(
    public dialog: MatDialog,
    private portfolioService: PortfolioService,
    private uiService: UiService
  ) {}

  userPortfolios$: Observable<any> = of([]);

  currentPortfolio$ = new BehaviorSubject<ExistingPortfolio>({
    portfolioId: 0,
    portfolioName: '',
    amount: 0.0,
    stock: [],
  });

  onCreatePortfolio() {
    this.dialog.open(CreatePortfolioComponent, {
      width: '30vw',
    });
  }

  ngOnInit() {
    this.portfolioService.init();
    this.userPortfolios$ = this.portfolioService.getPortfolios();
    this.portfolioService.getCurrentPortfolio().subscribe((response) => {
      this.currentPortfolio$.next(response);
      
    });
    
    
  }

  onDeletePortfolio(portfolioId: number): void {
    this.portfolioService.deletePortfolio(portfolioId).subscribe({
      next: (response) => {
        if (response) {
          localStorage.setItem('portfolio', '0');
          this.portfolioService.init();
          this.uiService.showSnackbar({
            message: response.message,
            status: 'success',
          });
        }
      },
      error: (error) => {
        this.uiService.showSnackbar({
          message: error.error.message,
          status: 'error',
        });
      },
    });
  }

  onSelectPortfolio(portfolio: ExistingPortfolio): void {
    this.portfolioService.setCurrentPortfolio(portfolio);

    this.userPortfolios$.forEach((p) => {
      let count: number = 0;
      p.forEach((port: ExistingPortfolio) => {
        if (port.portfolioName === portfolio.portfolioName) {
          localStorage.setItem('portfolio', String(count));
          return;
        }
        count += 1;
      });
    });
  }
}
