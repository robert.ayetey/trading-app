import { Component } from '@angular/core';
import { Trade } from 'src/app/models/trade.model';

const TRADES_DATA: Trade[] = [];

@Component({
  selector: 'app-admin-trades',
  templateUrl: './admin-trades.component.html',
  styleUrls: ['./admin-trades.component.css'],
})
export class AdminTradesComponent {
  displayedColumns: string[] = [
    'symbol',
    'side',
    'purchase_price',
    'quantity',
    'total_value',
    'status',
    'profit_or_loss',
    'customer',
    'portfolio',
  ];
  dataSource = TRADES_DATA;
}
