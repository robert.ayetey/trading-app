import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginData } from 'src/app/models/auth.model';
import { AuthService } from 'src/app/services/auth.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css'],
})
export class AdminLoginComponent {
  constructor(
    private authService: AuthService,
    private router: Router,
    private uiService: UiService
  ) {}

  ngOnInit() {
    document.body.classList.add('u-body-bg-light-auth');
  }

  ngOnDestroy() {
    document.body.classList.remove('u-body-bg-light-auth');
  }

  form: LoginData = {
    email: 'tlc3@turntabl.io',
    password: 'kantamanto',
  };

  onSubmit() {
    if (!this.form.email || !this.form.password) {
      this.uiService.showSnackbar({
        message: 'Please enter email and/or password',
        status: 'error',
      });
      return;
    }

    this.authService.login({ ...this.form }, 'admin').subscribe({
      next: (response) => {
        localStorage.setItem('user', JSON.stringify(response));
        localStorage.setItem('lgn', 'admin');
        this.router.navigate(['/admin/dashboard']);
        this.authService.setIsLoggedIn(true);
      },
      error: (error) =>
        this.uiService.showSnackbar({ message: error.error, status: 'error' }),
    });
  }
}
