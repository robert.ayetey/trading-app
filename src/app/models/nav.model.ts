export interface Nav {
  path: string;
  name: string;
}
