export interface Stock {
  stockId: number;
  ticker: string;
  quantity: number;
  value: number;
}
