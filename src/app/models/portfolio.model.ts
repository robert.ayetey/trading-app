import { Stock } from './stock.model';

export interface Portfolio {}

export interface NewPortfolio {
  portfolioName: string;
  amount: number;
  email: string;
}

export interface ExistingPortfolio {
  portfolioId: number;
  portfolioName: string;
  amount: number;
  stock: Stock[];
}
