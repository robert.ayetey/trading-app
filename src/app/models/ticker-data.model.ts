export interface RawTickerData {
  TICKER: string;
  SELL_LIMIT: number;
  LAST_TRADED_PRICE: number;
  MAX_PRICE_SHIFT: number;
  ASK_PRICE: number;
  BUY_LIMIT: number;
  exchange: any;
}

export interface TickerData {
  [key: string]: number[];
}
