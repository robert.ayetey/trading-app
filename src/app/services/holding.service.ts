import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HoldingService {

  constructor(private http:HttpClient) { }

  private url = environment.apiUrl;

  private userStock$ = new BehaviorSubject<any[]>([]);

  private currentPrices$ = new BehaviorSubject<any[]>([]);

  initStocks(portfolioId: number): void {
    this.http
      .get<any>(`${this.url}/client/stock/${portfolioId}`)
      .subscribe((response) => this.userStock$.next(response));
  }

  initCurrentPrices() 
  {
    this.http
      .get<any>(`${this.url}/client/prices`)
      .subscribe((response) => this.currentPrices$.next(response));
  }

  getAllStocks(): Observable<any> 
  {
    return this.userStock$;
  }

  getAllPrices(): Observable<any[]>
  {
    return this.currentPrices$
  }
}
