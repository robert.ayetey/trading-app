import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Order, OrderRequest } from '../models/order.model';
import { PortfolioService } from './portfolio.service';
import { UiService } from './ui.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': ['POST', 'GET', 'OPTIONS', 'DELETE', 'PUT'],
  }),
};

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  constructor(
    private http: HttpClient,
    private uiService: UiService,
    private portfolioService: PortfolioService
  ) {}

  private url = environment.apiUrl;

  private userOrders$ = new BehaviorSubject<any[]>([]);

  placeOrder(order: OrderRequest): Observable<any> {
    return this.http.post<any>(`${this.url}/client/order`, order, httpOptions);
  }

  initPendingPortfolios(portfolioId: number): void {
    this.http
      .get<any>(`${this.url}/client/${portfolioId}/orders/pending`)
      .subscribe((response) => this.userOrders$.next(response));
  }

  getAllPendingOrdersOfPortfolio() {
    return this.userOrders$;
  }

  cancelOrder(tradeId: String) {
    this.http.delete<any>(`${this.url}/client/order/${tradeId}`).subscribe({
      next: (response) => {
        if (response) {
          this.portfolioService.init();
          this.uiService.showSnackbar({
            message: response.message,
            status: 'success',
          });
        }
      },
      error: (error) => {
        this.uiService.showSnackbar({
          message: error.error.message,
          status: 'error',
        });
      },
    });
  }
}
