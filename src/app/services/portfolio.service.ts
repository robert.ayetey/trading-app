import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ExistingPortfolio, NewPortfolio } from '../models/portfolio.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': ['POST', 'GET', 'OPTIONS', 'DELETE', 'PUT'],
  }),
};

@Injectable({
  providedIn: 'root',
})
export class PortfolioService {
  private url = environment.apiUrl;

  private userPortfolios$ = new BehaviorSubject<ExistingPortfolio[]>([]);

  private currentPortfolio$ = new BehaviorSubject<ExistingPortfolio>({
    portfolioId: 0,
  portfolioName: "",
  amount: 0.0,
  stock: [],
  })


  constructor(private http: HttpClient) {}

  createPortfolio(newPortfolio: NewPortfolio): Observable<any> {
    return this.http.post<any>(
      `${this.url}/client/portfolio`,
      newPortfolio,
      httpOptions
    );
  }

  getPortfolios(): Observable<ExistingPortfolio[]> {
    return this.userPortfolios$;
  }

  getCurrentPortfolio(): Observable<ExistingPortfolio> {
    return this.currentPortfolio$;
  }

  setCurrentPortfolio(portfolio: ExistingPortfolio): void 
  {
    this.currentPortfolio$.next(portfolio);
  }


  init(): void {
    this.http
      .get<any>(
        `${this.url}/client/portfolio/${
          JSON.parse(localStorage.getItem('user') as string).clientId
        }`
      )
      .subscribe((response) => 
      {
        this.currentPortfolio$.next(response[Number(localStorage.getItem('portfolio'))]);
        this.userPortfolios$.next(response);
      })
  }

  deletePortfolio(portfolioId: number): Observable<any> {
    return this.http.delete<any>(`${this.url}/client/portfolio/${portfolioId}`, httpOptions);
  }
}
