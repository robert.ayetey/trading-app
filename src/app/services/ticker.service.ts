import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';



export interface Ticker {
  tickerLable: String;
}

@Injectable({
  providedIn: 'root'
})
export class TickerService {

  private url = environment.apiUrl;

  constructor(private http:HttpClient) { }

  getTickers() : Observable<Ticker> {
    return this.http
      .get<any>(
        `${this.url}/client/tickers`
      )
  }
}
