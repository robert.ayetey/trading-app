import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Holding } from 'src/app/models/holding.model';
import { CurrentPrice, PendingOrder } from 'src/app/models/order.model';
import { Stock } from 'src/app/models/stock.model';
import { HoldingService } from 'src/app/services/holding.service';
import { OrderService } from 'src/app/services/order.service';
import { PortfolioService } from 'src/app/services/portfolio.service';

interface SelectedStock {
  ticker: string;
  side: string;
}

@Component({
  selector: 'app-holdings',
  templateUrl: './holdings.component.html',
  styleUrls: ['./holdings.component.css'],
})
export class HoldingsComponent implements OnInit {
  constructor(
    private orderService: OrderService,
    private portfolioService: PortfolioService,
    private holdingService: HoldingService,
    private router: Router
  ) {}

  userPendingOrder$: Observable<any[]> = of([]);

  userStock$: Observable<any[]> = of([]);

  currentPrices$: Observable<any[]> = of([]);
  holdingColumns: string[] = [
    'tickerLable',
    'description',
    'currentPrice',
    'quantity',
    'value',
    'profit',
    // 'holding_actions',
  ];

  pendingColumns: string[] = [
    'ticker',
    'exchangeId',
    'exchangeName',
    'orderType',
    'side',
    'quantity',
    'price',
    'createdAt',
    'trade_actions',
  ];

  COLS: any = {
    exchangeId: 'order id',
    exchangeName: 'exchange',
    orderType: 'type',
    createdAt: 'date',
    trade_actions: 'actions',
    holding_actions: 'actions',
    tickerLable: 'ticker',
    currentPrice: 'current price',
  };

  pendingSource: PendingOrder[] = [];
  holdingSource: Stock[] = [];

  private selectedStock$ = new BehaviorSubject<any>({});

  ngOnInit() {
    this.portfolioService.getCurrentPortfolio().subscribe((response) => {
      if (response.portfolioId != 0) {
        this.orderService.initPendingPortfolios(response.portfolioId);
        this.holdingService.initStocks(response.portfolioId);
      }
    });
    //current prices
    this.holdingService.initCurrentPrices();
    this.currentPrices$ = this.holdingService.getAllPrices();

    //user stocks
    this.userStock$ = this.holdingService.getAllStocks();
    this.userStock$.subscribe((response) => {
      this.holdingSource = response;
    });

    //user pending orders
    this.userPendingOrder$ = this.orderService.getAllPendingOrdersOfPortfolio();
    this.userPendingOrder$.subscribe((response) => {
      this.pendingSource = response;
    });
  }

  cancelOrder(exchangeId: String) {
    this.orderService.cancelOrder(exchangeId);
  }

  createSellOrder(holding: Holding) {
    this.router.navigate(['/client/trade']);
  }

  createBuyOrder(holding: Holding) {
    this.router.navigate(['/client/trade']);
  }

  getCurrentPriceFor(ticker: string): string {
    let ret = '-';
    this.currentPrices$.forEach((subs) => {
      subs.forEach((item) => {
        if (item.tickerLable == ticker) {
          ret = item.askPrice;
        }
      });
    });
    return ret;
  }

  parseToInt(str: string): number {
    let ret: number = +str;
    return ret;
  }

  calcProfit(element: any): number {
    return +(
      ((this.parseToInt(
        this.getCurrentPriceFor(element['ticker'].tickerLable)
      ) *
        element['quantity'] -
        element['value']) /
        element['value']) *
      100
    ).toFixed(2);
  }
}
