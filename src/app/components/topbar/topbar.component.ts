import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Nav } from 'src/app/models/nav.model';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css'],
})
export class TopbarComponent {
  @Input() userType: 'admin' | 'client' = 'client';

  constructor(private router: Router) {}

  getCurrentRoute() {
    return this.router.url;
  }

  clientNav: Nav[] = [
    { path: '/client/dashboard', name: 'Dashboard' },
    { path: '/client/trade', name: 'Trade' },
    { path: '/client/market', name: 'Market' },
  ];

  adminNav: Nav[] = [
    { path: '/admin/dashboard', name: 'Dashboard' },
    { path: '/admin/trades', name: 'Trades' },
    { path: '/admin/market', name: 'Market' },
    { path: '/admin/configure-system', name: 'Configure System' },
  ];

  onLogout(): void {
    localStorage.clear();
    window.location.replace('/');
  }
}
