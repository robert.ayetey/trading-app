import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Order } from 'src/app/models/order.model';
import { OrderService } from 'src/app/services/order.service';
import { PortfolioService } from 'src/app/services/portfolio.service';
import { UiService } from 'src/app/services/ui.service';

@Component({
  selector: 'app-preview-trade-order',
  templateUrl: './preview-trade-order.component.html',
  styleUrls: ['./preview-trade-order.component.css'],
})
export class PreviewTradeOrderComponent {
  constructor(
    public dialogRef: MatDialogRef<PreviewTradeOrderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Order,
    private orderService: OrderService,
    private portfolioService: PortfolioService,
    private uiService: UiService
  ) {}

  estimatedPrice: number = 20;

  onClose(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.orderService
      .placeOrder({ ...this.data, portfolioId: this.data.portfolio.id })
      .subscribe({
        next: (response) => {
          if (response) {
            this.portfolioService.init();
            this.uiService.showSnackbar({
              message: response.message,
              status: 'success',
            });
          }
        },
        error: (error) => {
          this.uiService.showSnackbar({
            message: error.error.message,
            status: 'error',
          });
        },
      });
    this.onClose();
  }
}
